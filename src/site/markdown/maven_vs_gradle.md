# Maven vs Gradle
Da jeg har brukt Gradle en del før er ikke konseptet med et prosjektverktøy ukjent. Begge lar oss definere og konfigurere flere av aspektene for hva som trengs å gjøres for å bygge prosjektet vårt. Maven kom først og har vært her en stund, Gradle er relativt nytt og tar mye av grunnlaget sitt fra Maven.

Selv om de er ganske like, og i teorien lar oss gjøre det samme, må jeg bare få si at Maven har vært en smertefull affære. Det første man møter som ny bruker av Maven er dokumentasjonen, som jeg føler har en 'ovenfra og ned'-holdning til nye brukere og ikke er spesielt inviterende til utforskning.

Med dette tenker jeg både på hvordan man skal konfigurere `pom.xml`, men også hvilke funksjoner/plugins som er tilgjengelig via `mvn`-kommandoen. Det har vært som å bli plassert i et ukjent, mørkt rom hvor det forventes at du skal vite hvor du er og hvor du skal.

Da jeg ikke har mye pent å si om Maven skal jeg holde dette dokumentet kort.

## Dokumentasjonen
Gradle sin dokumentasjon er ikke perfekt, men den har en positiv holdning til nye brukere og tar deg stegvis ut på dypere vann ved å forklare og vise funksjoner og muligheter.  
Maven, derimot, kaster deg umiddelbart på dypt vann og ber deg få fatt i redningsbøya selv.

Det er ikke alltid at dette er negativt, men da dette er et såpass utbredt verktøy ble jeg overrasket over hvor elendig den offisielle dokumentasjonen er. Det er ganske unødvendig å måtte google seg fram til svar på enkle problemer som burde vært representert i dokumentasjonen.

## Config
Maven benytter XML, Gradle benytter Groovy.  
Med Maven konfigurerer man programmet, med Gradle programmerer man konfigurasjonen. *(Ikke helt korrekt, men nærme nok for å få frem poenget mitt)*

Gradle bygger sin konfigurasjon utifra språket Groovy, som i teorien betyr at man kaller på funksjoner i filen `build.gradle` for å definere opp prosjektet ditt. Man har også mulighet til å definere egne tasks og plugins i denne filen som kan kjøres via kommandolinjen. Er ikke det fint? :)

**Dependencies** er en svært viktig del av både Maven og Gradle, og standard benytter begge MavenCentral for å søke opp og laste ned pakker.

Men forskjellen kommer til hvordan du definerer avhengighetene. Ta JUnit som eksempel, hvor du med Maven er nødt til å hoste opp 6(!) linjer:  

``` .xml
<dependency>
	<groupId>junit</groupId>
	<artifactId>junit</artifactId>
	<version>4.12</version>
	<scope>test</scope>
</dependency>
```

Mens Gradle nøyer seg med 1:  
`testCompile 'junit:junit:4.12'`

Med 10 dependencies til et prosjekt, som ikke er altfor mye, kan man ende opp med 60 linjer i Maven mot 10 i Gradle. Ikke misforstå, jeg mener ikke at kvaliteten til et verktøy kommer an på innhold/struktur config-filen, jeg mener bare at XML er et ekstremt dårlig valg når det kommer til oversiktlighet. Fordelen med XML er jo helt klart å kunne opprettholde og beskytte strukturen/integriteten i DOM'en ved hjelp av en schema-definisjon, men jeg syns ikke dette skal gå foran lesbarhet.

## Konklusjon
Dersom jeg får valget vil utfallet alltid være Gradle.
