# Dokumentasjon
Dette er ment som noe mer inngående dokumenasjon på valgene jeg har gjort gjennom innleveringen.

## Valg av rammeverk
Som nevnt i [notatene](notes.html) valgte jeg å benytte Spring Boot som rammeverk. Rammeverket lar meg raskt få stablet en web-applikasjon på bena, med lite konfigurasjon. Jeg inkluderte også et par ekstra pakker, bl.a. en for å benytte template-enginen Thymeleaf og én for å kun benytte Log4j til logging. Standard inkluderer Spring Boot egne klasser for logging, som benytter Log4j under panseret, men dette var ikke hva jeg ønsket.


## Logging
Til logging benytter jeg Log4j, og denne er konfigurert via filen `src/main/resources/log4j2.yml`.

Årsaken til at jeg valgt YAML framfor XML er lesbarheten. Fordelen er lesbarheten, ulempen er at det fins temmelig nøyaktig 0 linjer dokumenasjon om dette, til tross for at det er støtte for det. Heia, heia!

### log4j2.yml
Jeg har definert opp 3 loggere: to til fil og en til konsoll.

Til konsoll skrives alle meldinger som er level `debug` eller høyere.

Til den ene filen skrives kun level `debug` og til den andre kun `error`.

Plasseringen av filene er til nå satt til mappen `/tmp/pg4100.innlevering3/`, da `/tmp` er en mappe alle brukere har tillatelsen til å skrive til. Denne står man fritt til å endre selv, dersom man syns det er en dårlig plassering. *(Noe det helt klar er)*

Da Thymeleaf og HTMLUnitDriver var noe irriterende i konsollen under test og kjøring valgte jeg å ekskludere alle innslag i loggen som er i nivå under `error` fra alt som ligger under disse pakkene.

### Når logges det?
Alle forespørsler logges i controlleren sammen med resultatet av primtallssjekken.

Exceptions logges dersom det inntreffer noe som helst sted under applikasjonens kjøretid.


## Klienten
Klienten i min løsning er en enkel HTML-side med et enkelt input. Da det ikke er noe mer fancy hopper jeg over noen dypere forklaring her.

Jeg kan nevne at jeg brukte Thymeleaf til templating. På den måten slapp jeg unna med å bruke et enkelt template.

## Server
### Generelt
Serveren er Spring Boot-applikasjonen jeg har nevnt over. Den består av 1 controller som besvarer forespørsler mot `/`, og benytter to metoder til å besvare forespørselen, avhengig om det er via `GET` eller `POST`.

### Input og validering
Ved innsendelse, og dermed via `POST`, blir input sjekket om det er sendt en tom streng eller en streng bestående av andre tegn enn tall. Hvis dette er tilfelle får bruker beskjed om hva som må rettes.

Dersom input er kun tall sjekkes det om det er et primtall, og bruker får beskjed om resultatet.

### Primtallssjekk
Input fra bruker kommer i form av en `String`. For å finne ut om dette er et primtall må det gjøres om til en `int`.

#### `Integer` vs `BigInteger`
Her møtte jeg på et valg: skal jeg benytte `Integer.parseInt(String s)` eller `BigInteger`? Jeg gikk for å bruke `BigInteger`.  
Ved å benytte `Integer` får jeg en [øvre grense på 2<sup>31</sup>-1][1], mens med `BigInteger` får jeg (antageligvis) [en langt høyere grense][2].

Dersom brukeren har tastet inn en verdi som overstiger `Integer.MAX_VALUE` vil det bli kastet en exception av type `NumberFormatException` dersom jeg forsøker å parse dette inputet. Ved bruk av `BigInteger` vil det ikke det.

`BigInteger` benytter en helt annen måte å lagre tall på og har en [øvre grense på 2<sup>`Integer.MAX_VALUE`</sup>][3]. Men dersom man ønsker å hente ut en vanlig `int` vil `BigInteger.intValue()` kun returnere de nederste 32-bitsene, dersom verdien overstiger `Integer.MAX_VALUE`.

Ulempen med dette er at jeg ikke får gitt bruker en korrekt tilbakemelding på at han/hun har skrevet inn en verdi som overstiger hva applikasjonen klarer å håndtere, samtidig som verdien som sendes videre til sjekk fra `BigInteger.intValue()` alltid vil være 2.147.483.647 da kun de 32 nederste bitsene sendes videre. Dette er ikke en gunstig løsning da jeg gir bruker et villedende svar, men jeg føler det blir en corner-case hvor man bare må ta et standpunkt.

**EDIT**  
Løsningen er skrevet om og tester er lagt til slik at bruker får beskjed om at `Integer.MAX_VALUE` er det største som kan sjekkes. Dersom det tastes inn en verdi over dette er det ingen vits i å sjekke om det er et primtall, da `BigInteger.intValue()` som nevnt kun returnerer de 32 nederste bitsene.

#### Selve sjekken
Sjekken av primtall gjøres av biblioteket [`commons-math3`][4] fra Apache Commons, da via klassen `org.apache.commons.math3.primes.Primes`. Biblioteket er satt som avhengighet via prosjektets `pom.xml`.

### Respons til klient
Etter at et resultat er generert via klassen `PrimeChecker` bygges det opp en respons til bruker, samt at resultat loggføres til debug-loggen som tidligere er nevnt.

For å kommunisere en beskjed tilbake til brukeren sender jeg ved en `String` til viewet som vises via Thymeleaf.

## Testing
I motsetning til tidligere har jeg denne gangen både enhetstester og integrasjonstester. Integrasjonstestene er skrevet ved hjelp av Cucumber, et bibliotek som lar meg beskrive en gitt funksjon i applikasjonen min fra en definert brukers ståsted, dette ved hjelp av formatet/språket de kaller 'gherkin'.

[1]: https://docs.oracle.com/javase/8/docs/api/java/lang/Integer.html
[2]: http://stackoverflow.com/questions/12693273/is-there-an-upper-bound-to-biginteger
[3]: https://docs.oracle.com/javase/8/docs/api/java/math/BigInteger.html
[4]: https://commons.apache.org/proper/commons-math/
