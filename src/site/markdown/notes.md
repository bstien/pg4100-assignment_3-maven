# Innlevering 3: Maven
_Levert av Bastian Stien_

## Om oppgaven
Innleveringen bestod av å stable en klient og server på plass ved hjelp av Maven. Via klienten skulle man kunne sende inn et tall til serveren for så å få et svar tilbake om det man sendte var et primtall eller ei. Ved innsendelse hvor input ikke var tall skulle serveren svare med en melding som indikerte hva som var korrekt input.

Hver request skulle loggføres i egen fil sammen med resultatet av primtallssjekken og ev. exceptions skulle logges i en annen. Loggeren skulle være Log4j, eller lignende.

## Hva har jeg lært?
I denne innleveringen har jeg lært å benytte Maven som prosjektverktøy. Tidligere har jeg til Java-prosjekter brukt [Gradle][1] og til PHP brukt [Composer][2], så konseptet var jeg kjent med. I motsetning til Gradle, som bruker Groovy, og Composer, som bruker JSON, bruker Maven XML til å definere prosjektet man arbeider med via filen `pom.xml`.

Jeg valgte å benytte [Spring Boot][3] som rammeverk for løsningen min, da dette har vært noe jeg har hatt lyst til å prøve en stund. Ved hjelp av Spring får jeg raskt tilgang til å sette opp controllere og routes via annotasjoner. I tillegg inkluderes det en embedded Tomcat-server som gjør at man slipper å deploye applikasjonen for å teste endringer, alt som skal til er å kjøre prosjektet eller bruke kommandoen `mvn spring-boot:run`. Default blir applikasjonen tilgjengelig på `http://localhost:8080`.

Til logging valgte jeg å benytte [Log4j][4], da jeg har skjønt at dette er et valg som passer de fleste situasjoner da den kan konfigureres.

Til testing valgte jeg også å inkludere integrasjonstester ved hjelp av  [Cucumber][5] og [Selenium][6]. Cucumber lar meg skrive features løsningen min skal ha, fra en gitt bruker eller rolles ståsted. Det jeg ser som den store fordelen med Cucumber og dens feature-filer er at de er lesbare av alle på et team. Både ledere, selgere, designere og programmerere kan både lese og skrive slike filer, noe som gjør at alle vil være på samme side når det kommer til å definere features og teste at disse fungerer.

Selenium automatiserer nettlesere og lar oss interagere med en nettside f.eks. under testing.


## Hva kunne vært gjort bedre?
Jeg har ingen store tanker om noe som bør endres på.

## Forutsetninger for å kjøre
### Embedded Tomcat
For å teste applikasjonen via den embeddede Tomcat-serveren:  
`mvn spring-boot:run`

Gå så til adressen `http://localhost:8080` i nettleseren.

### Tomcat: manuell deploy  
Benytt denne metoden dersom du ønsker å deploye war-filen selv.

Fjern ev. andre spor av en tidligere build og bygg prosjektet på nytt:  
`mvn clean install`

I mappen `target/` eksisterer nå en fil kalt `20160417_maven-1.0.war`. Denne er klar for flytting til lokal Tomcat-installasjon.

### Tomcat: automatisk deploy  
For å automatisk bygge, samt deploye war til lokal Tomcat-installasjon er det et par ting som må sjekkes først. I `pom.xml` har jeg helt nederst inkludert pluginen `tomcat7-maven-plugin`. Påse at informasjonen under `<configuration>` passer med ditt oppsett.

#### Definere Tomcat-bruker
**PS**! Dersom du allerede har definert en server med tilhørende url og credentials under `~/.m2/settings.xml` kan du føre inn navnet du har gitt denne serveren i `<server>`-tagen og [gå videre til deploy](#deploy)

Hvis du ikke har dette konfigurert må dette inn:

I Tomcat sin `tomcat-users.xml` må du påse at du har minst 1 bruker med rollen `manager-script`. Denne brukerens brukernavn og passord er hva du fører inn under `<username>` og `<password>`.

Hvis denne brukeren ikke er definert kan du lime inn dette i `tomcat-users.xml`:  


``` .xml
<role rolename="manager-script"/>
<user username="admin" password="admin" roles="manager-script"/>
```


#### Konfigurere `pom.xml`
`<url>`: peker til adressen og porten din lokale Tomcat kjører på. Default bruker Tomcat `http://localhost:8080`. Pathen `/manager/text` peker til stien manager-applikasjonen kjører på, og må ikke fjernes/endres.

`<username>`: brukernavnet du har definert for brukeren med rollen `manager-script`.

`<password>`: passordet du har definert for brukeren med rollen `manager-script`.

`<path>`: Hvilken path/context som skal applikasjonen kjøre i. Jeg har satt `/bstien` og da vil den bli tilgjengelig på `http://localhost:8080/stibas14`, med mindre Tomcat kjører på en annen adresse.

`<server>`: Et arbitrært navn på serveren vi nå konfigurerer. Vil peke til en definert server i filen `~/.m2/settings.xml`. Hvis du ikke har noe definert på forhånd har det ingenting å si hva du fører inn her.

#### Deploy
Hvis det er første gangen du deployer denne applikasjonen kjører du denne kommandoen:  
`mvn tomcat7:deploy`

Ved hver deploy etter dette må denne kjøres:  
`mvn tomcat7:redeploy`

[1]: http://gradle.org/
[2]: https://getcomposer.org/
[3]: http://projects.spring.io/spring-boot/
[4]: http://logging.apache.org/log4j/2.x/
[5]: https://cucumber.io/
[6]: http://www.seleniumhq.org/
