# PG4100, Innlevering 3: Maven

**Innlevert av**  
Bastian Stien, [stibas14@student.westerdals.no][1]  


Dette er min besvarelse til innlevering 3 i faget PG4100, Avansert Javaprogrammering 2.

**Deploybar .war-fil** ligger i innleveringens rotmappe. Dokumentasjonen, som du tydeligvis har funnet, er generert og ligger i mappen `site`.

**Notater til innleveringen**, samt beskrivelse av hva som må gjøres for å kjøre applikasjonen ligger under siden [notater](notes.html).

**Inngående dokumenasjon** finnes [her](documentation.html).

'**Maven vs. Gradle**' er [her](maven_vs_gradle.html).

**JavaDocs** er [her](apidocs/index.html).

[1]: mailto:stibas14@student.westerdals.no
