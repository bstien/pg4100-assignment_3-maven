Feature: Checking if a number is a prime number
  As a user
  I want to enter a number into the page
  In order to see if it's a prime

  Scenario: The number is a prime
    Given I am on the path "/"
    And I enter "37" into the field "number"
    And submit the form
    Then the message should be "37 er et primtall"

  Scenario: The number is not a prime
    Given I am on the path "/"
    And I enter "10" into the field "number"
    And submit the form
    Then the message should be "10 er ikke et primtall"

  Scenario: The input is not a number
    Given I am on the path "/"
    And I enter "not a number" into the field "number"
    And submit the form
    Then the message should be "Input kan kun bestå av tall"

  Scenario: The input is empty
    Given I am on the path "/"
    And I enter "" into the field "number"
    And submit the form
    Then the message should be "Input kan ikke være en tom streng"

  Scenario: The input is empty
    Given I am on the path "/"
    And I enter "999999999999999" into the field "number"
    And submit the form
    Then the message should match "\d+ er den største verdien jeg klarer å sjekke"