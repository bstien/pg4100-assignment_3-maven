package stibas14.pg4100.assignments.maven.features;

import cucumber.api.PendingException;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;

import static org.junit.Assert.assertEquals;

public class CheckPrimeNumberSteps implements En {

	private HtmlUnitDriver htmlDriver;

	public CheckPrimeNumberSteps()
	{
		Given("^I am on the path \"([^\"]*)\"$", (String path) ->
		{
			htmlDriver.get("http://localhost:9000" + path);
		});

		And("^I enter \"([^\"]*)\" into the field \"([^\"]*)\"$", (String input, String fieldName) ->
		{
			WebElement inputField = htmlDriver.findElement(By.name(fieldName));
			inputField.sendKeys(input);
		});

		And("^submit the form$", () ->
		{
			WebElement submitButton = htmlDriver.findElement(By.xpath("/html/body/div/div[2]/div/form/div[2]/input"));
			submitButton.sendKeys(Keys.ENTER);
		});

		Then("^the message should be \"([^\"]*)\"$", (String expectedMessage) ->
		{
			WebElement messageElement = htmlDriver.findElement(By.id("message"));
			String actualMessage = messageElement.getText();

			assertEquals(expectedMessage, actualMessage);
		});

		Then("^the message should match \"([^\"]*)\"$", (String regex) -> {
			WebElement messageElement = htmlDriver.findElement(By.id("message"));
			String message = messageElement.getText();

			boolean matchesRegex = message.matches(regex);

			assertEquals(true, matchesRegex);
		});

	}

	@Before
	public void setUp() throws Exception
	{
		htmlDriver = new HtmlUnitDriver();
	}

	@After
	public void tearDown() throws Exception
	{
		htmlDriver.quit();
	}
}
