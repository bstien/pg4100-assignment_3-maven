package stibas14.pg4100.assignments.maven.domain;

import org.junit.Test;

import static org.junit.Assert.*;
import static stibas14.pg4100.assignments.maven.domain.InputValidator.numbersOnly;

public class InputValidatorTest {

	@Test
	public void it_should_return_true_when_only_numbers_are_given_to_numbers_only_method() throws Exception
	{
		assertTrue(numbersOnly("123"));
		assertTrue(numbersOnly("1"));
	}

	@Test
	public void it_should_return_false_when_other_characters_are_given_to_numbers_only_method() throws Exception
	{
		assertFalse(numbersOnly("123a"));
		assertFalse(numbersOnly("a1"));
	}
}