package stibas14.pg4100.assignments.maven.domain;

import org.apache.commons.math3.primes.Primes;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class PrimeCheckerTest {

	private String validInputWithPrime;

	private String validInputWithComposite;

	private String validInput;

	private String invalidInput;

	private String integerMaxInput;

	private String tooLargeInput;

	@Before
	public void setUp() throws Exception
	{
		validInputWithPrime = "" + Primes.nextPrime(10);
		validInputWithComposite = "10";
		validInput = "123456789";
		invalidInput = "invalid input";
		integerMaxInput = "" + Integer.MAX_VALUE;
		tooLargeInput = "" + Integer.MAX_VALUE + 1;
	}

	@Test
	public void it_should_return_a_positive_result_when_a_prime_is_given() throws Exception
	{
		PrimeResult result = PrimeChecker.check(validInputWithPrime);

		assertEquals(true, result.isPrime());
	}

	@Test
	public void it_should_return_a_negative_result_when_a_composite_is_given() throws Exception
	{
		PrimeResult result = PrimeChecker.check(validInputWithComposite);

		assertEquals(false, result.isPrime());
	}

	@Test
	public void it_should_recognize_a_valid_input() throws Exception
	{
		PrimeResult result = PrimeChecker.check(validInput);

		assertEquals(true, result.isValidInput());
	}

	@Test
	public void it_should_recognize_an_invalid_input() throws Exception
	{
		PrimeResult result = PrimeChecker.check(invalidInput);

		assertEquals(false, result.isValidInput());
	}

	@Test
	public void it_should_recognize_a_number_that_is_less_than_integer_max_value() throws Exception
	{
		PrimeResult result = PrimeChecker.check(integerMaxInput);

		assertEquals(false, result.isTooLarge());
	}

	@Test
	public void it_should_recognize_a_number_that_is_too_large_to_handle() throws Exception
	{
		PrimeResult result = PrimeChecker.check(tooLargeInput);

		assertEquals(true, result.isTooLarge());
	}
}