package stibas14.pg4100.assignments.maven.domain;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * A class to validate input given from a user based on regex.
 */
public class InputValidator {

	/**
	 * Only allow digits in input.
	 *
	 * @param input A string
	 * @return True if only numbers, false otherwise
	 */
	public static boolean numbersOnly(String input)
	{
		String regex = "^(\\d+)$";

		return isValid(input, regex);
	}

	/**
	 * Match a string against regex.
	 *
	 * @param input The String to check
	 * @param regex The regex
	 * @return True if match, false otherwise
	 */
	public static boolean isValid(String input, String regex)
	{
		Pattern pattern = Pattern.compile(regex);
		Matcher matcher = pattern.matcher(input);

		return matcher.matches();
	}
}
