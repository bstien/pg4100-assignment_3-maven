package stibas14.pg4100.assignments.maven.domain;

import stibas14.pg4100.assignments.maven.log.Loggable;

/**
 * A class to represent the result from a prime-check on a number.
 */
public class PrimeResult implements Loggable {

	private String input;

	private boolean isValidInput;

	private boolean isPrime;

	private String message;

	private boolean tooLarge;

	public PrimeResult()
	{
		this("", false, false, false);
	}

	public PrimeResult(String input, boolean isValidInput, boolean isPrimeNumber, boolean tooLarge)
	{
		this.input = input;
		this.isValidInput = isValidInput;
		this.isPrime = isPrimeNumber;
		this.tooLarge = tooLarge;
		this.message = null;
	}

	public String getInput()
	{
		return input;
	}

	public PrimeResult setInput(String input)
	{
		this.input = input;
		return this;
	}

	public boolean isValidInput()
	{
		return isValidInput;
	}

	public PrimeResult setIsValidInput(boolean validInput)
	{
		isValidInput = validInput;
		return this;
	}

	public boolean isPrime()
	{
		return isPrime;
	}

	public PrimeResult setIsPrime(boolean primeNumber)
	{
		isPrime = primeNumber;
		return this;
	}

	@Override
	public String getMessage()
	{
		if ( message == null )
		{
			StringBuilder builder = new StringBuilder();
			builder.append("input: \"" + input + "\", ");
			builder.append("valid: " + isValidInput() + ", ");
			builder.append("prime: " + isPrime() + ", ");
			builder.append("over int max: " + isTooLarge());

			message = builder.toString();
		}

		return message;
	}

	public boolean isTooLarge()
	{
		return tooLarge;
	}

	public void setTooLarge(boolean tooLarge)
	{
		this.tooLarge = tooLarge;
	}
}
