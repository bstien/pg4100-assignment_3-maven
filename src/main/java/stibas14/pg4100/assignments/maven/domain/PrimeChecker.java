package stibas14.pg4100.assignments.maven.domain;

import org.apache.commons.math3.primes.Primes;

import java.math.BigInteger;

/**
 * A class to help check if a String containing digits is a prime.
 */
public class PrimeChecker {

	/**
	 * Check if the given String is a prime. Returns an instance of PrimeResult,
	 * which contains the results regarding valid input and whether it's a prime
	 * or not.
	 *
	 * @param input A string
	 * @return A PrimeResult object
	 */
	public static PrimeResult check(String input)
	{
		PrimeResult primeResult = new PrimeResult();

		// If the input is empty, there is nothing to do.
		if ( !input.equals("") )
		{
			// Check that we have valid input.
			boolean validInput = InputValidator.numbersOnly(input);
			primeResult.setInput(input).setIsValidInput(validInput);

			// If the input is valid, see if it's a prime.
			if ( validInput )
			{
				BigInteger number = new BigInteger(input);

				// See if the input is greater than Integer.MAX_VALUE.
				// If this is true, there is no way now to accurately
				// check if it is a prime, since BigInteger.intValue()
				// truncates to the lowest 32 bits.
				BigInteger integerMax = new BigInteger("" + Integer.MAX_VALUE);
				if ( number.compareTo(integerMax) == 1 )
				{
					primeResult.setTooLarge(true);
				}
				else
				{
					boolean isPrime = Primes.isPrime(number.intValue());
					primeResult.setIsPrime(isPrime);
				}
			}
		}

		return primeResult;
	}
}
