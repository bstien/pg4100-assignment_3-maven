package stibas14.pg4100.assignments.maven.log;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * A class to simplify debug logging throughout an application. As of now this
 * is a pretty unnecessary, but I wanted to try to separate the logging logic
 * from the controllers.
 */
public class DebugLogger {

	private Logger logger;

	/**
	 * Create a logger from a name.
	 *
	 * @param name
	 */
	public DebugLogger(String name)
	{
		logger = LogManager.getLogger(name);
	}

	/**
	 * Create a logger from a classname.
	 *
	 * @param clazz
	 */
	public DebugLogger(Class<?> clazz)
	{
		logger = LogManager.getLogger(clazz);
	}

	/**
	 * Get a default logger.
	 */
	public DebugLogger()
	{
		this(DebugLogger.class.toString());
	}

	/**
	 * Log a message.
	 *
	 * @param message
	 */
	public void log(String message)
	{
		logger.debug(message);
	}

	/**
	 * Log a message given from a Loggable-object.
	 *
	 * @param loggable
	 */
	public void log(Loggable loggable)
	{
		String message = loggable.getMessage();
		log(message);
	}
}
