package stibas14.pg4100.assignments.maven.log;

/**
 * A simple contract to be implemented by objects to simplify their
 * log-messages.
 */
public interface Loggable {

	String getMessage();
}
