package stibas14.pg4100.assignments.maven.web;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import stibas14.pg4100.assignments.maven.domain.PrimeChecker;
import stibas14.pg4100.assignments.maven.domain.PrimeResult;
import stibas14.pg4100.assignments.maven.log.DebugLogger;

/**
 * A class to handle all incoming requests regarding checking of primenumbers.
 */
@Controller
@RequestMapping("/")
public class PrimeNumbersController {

	DebugLogger logger = new DebugLogger(PrimeNumbersController.class);

	@RequestMapping(method = RequestMethod.GET)
	public String getIndex()
	{
		return "index";
	}

	@RequestMapping(method = RequestMethod.POST)
	public ModelAndView postIndex(@RequestParam(name = "number") String input)
	{
		ModelAndView mav = new ModelAndView("index");
		String viewMessage = null;
		PrimeResult primeResult = PrimeChecker.check(input);

		if ( primeResult.getInput().equals("") )
		{
			viewMessage = "Input kan ikke være en tom streng";
		}
		else
		{
			if ( primeResult.isValidInput() )
			{
				String number = primeResult.getInput();

				if ( primeResult.isTooLarge() )
				{
					viewMessage = "" + Integer.MAX_VALUE + " er den største verdien jeg klarer å sjekke";
				}
				else
				{
					boolean isPrime = primeResult.isPrime();

					String message = number + " er " + (isPrime ? "" : "ikke") + " et primtall";
					viewMessage = message;
				}
			}
			else
			{
				viewMessage = "Input kan kun bestå av tall";
			}
		}

		logger.log(primeResult);
		mav.addObject("message", viewMessage);
		return mav;
	}
}
